# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/powermgr/battery_manager/batterymgr.gni")
import("//build/test.gni")
import("//drivers/adapter/uhdf/uhdf.gni")

module_output_path = "battery_manager_native/batterysrv"

###############################################################################
config("module_private_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "include",
    "${battery_manager_path}/charger/ui/include",
    "${battery_manager_path}/charger/led",
    "${battery_manager_path}/charger/backlight",
    "${battery_manager_path}/charger/vibrate",
    "${battery_manager_path}/charger/include",
    "${battery_manager_path}/services/native/include",
    "${battery_manager_path}/interfaces/innerkits/native/include",
    "//drivers/peripheral/powermgr/battery/interfaces/include",
    "//drivers/peripheral/powermgr/battery/include",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/include",
  ]
}

##############################unittest##########################################
ohos_unittest("test_hdisrv") {
  module_out_path = module_output_path

  sources = [
    "//base/powermgr/battery_manager/charger/backlight/battery_backlight.cpp",
    "//base/powermgr/battery_manager/charger/led/battery_led.cpp",
    "//base/powermgr/battery_manager/charger/src/charger_thread.cpp",
    "//base/powermgr/battery_manager/charger/vibrate/battery_vibrate.cpp",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/src/battery_config.cpp",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/src/battery_thread.cpp",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/src/power_supply_provider.cpp",
    "src/hdi_service_test.cpp",
  ]

  configs = [
    "${utils_path}:utils_config",
    ":module_private_config",
  ]

  deps = [
    "${battery_manager_path}/interfaces/innerkits:batterysrv_client",
    "//base/powermgr/battery_manager/charger/ui:libminui",
    "//base/powermgr/battery_manager/services:batteryservice",
    "//base/powermgr/power_manager/interfaces/innerkits:powermgr_client",
    "//base/startup/syspara_lite/interfaces/innerkits/native/syspara:syspara",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi/battery/v1_0:libbattery_proxy_1.0",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi/battery/v1_0:libbattery_stub_1.0",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_callback:libbattery_callback",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//third_party/libpng:libpng",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "ability_runtime:base",
    "ability_runtime:want",
    "bundle_framework:appexecfwk_base",
    "ces_standard:cesfwk_core",
    "ces_standard:cesfwk_innerkits",
    "device_driver_framework:libhdf_host",
    "device_driver_framework:libhdf_ipc_adapter",
    "device_driver_framework:libhdf_utils",
    "eventhandler:libeventhandler",
    "hiviewdfx_hilog_native:libhilog",
    "input_device_driver:hdi_input",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr_standard:samgr_proxy",
  ]
}

##############################unittest##########################################
ohos_unittest("test_capacity_shutdown") {
  module_out_path = module_output_path

  sources = [
    "//base/powermgr/battery_manager/charger/backlight/battery_backlight.cpp",
    "//base/powermgr/battery_manager/charger/led/battery_led.cpp",
    "//base/powermgr/battery_manager/charger/src/charger_thread.cpp",
    "//base/powermgr/battery_manager/charger/vibrate/battery_vibrate.cpp",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/src/battery_config.cpp",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/src/battery_thread.cpp",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_service/src/power_supply_provider.cpp",
    "src/capacity_shutdown_test.cpp",
  ]

  configs = [
    "${utils_path}:utils_config",
    ":module_private_config",
  ]

  deps = [
    "${battery_manager_path}/interfaces/innerkits:batterysrv_client",
    "//base/powermgr/battery_manager/charger/ui:libminui",
    "//base/powermgr/battery_manager/services:batteryservice",
    "//base/powermgr/power_manager/interfaces/innerkits:powermgr_client",
    "//base/startup/syspara_lite/interfaces/innerkits/native/syspara:syspara",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi/battery/v1_0:libbattery_proxy_1.0",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi/battery/v1_0:libbattery_stub_1.0",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_callback:libbattery_callback",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//third_party/libpng:libpng",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "device_driver_framework:libhdf_host",
    "device_driver_framework:libhdf_ipc_adapter",
    "device_driver_framework:libhdf_utils",
    "hiviewdfx_hilog_native:libhilog",
    "input_device_driver:hdi_input",
    "ipc:ipc_single",
  ]
}

##############################unittest##########################################
ohos_unittest("test_input_event") {
  module_out_path = module_output_path

  include_dirs =
      [ "${battery_manager_path}/interfaces/innerkits/native/include" ]

  sources = [ "src/hdi_input_test.cpp" ]

  configs = [
    "${utils_path}:utils_config",
    ":module_private_config",
  ]

  deps = [
    "${battery_manager_path}/interfaces/innerkits:batterysrv_client",
    "//base/powermgr/battery_manager/charger/ui:libminui",
    "//base/powermgr/battery_manager/services:batteryservice",
    "//base/powermgr/power_manager/interfaces/innerkits:powermgr_client",
    "//base/startup/syspara_lite/interfaces/innerkits/native/syspara:syspara",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi/battery/v1_0:libbattery_proxy_1.0",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi/battery/v1_0:libbattery_stub_1.0",
    "//drivers/peripheral/powermgr/battery/interfaces/hdi_callback:libbattery_callback",
    "//third_party/googletest:gmock_main",
    "//third_party/googletest:gtest_main",
    "//third_party/jsoncpp:jsoncpp",
    "//utils/native/base:utils",
  ]

  external_deps = [
    "device_driver_framework:libhdf_host",
    "device_driver_framework:libhdf_ipc_adapter",
    "device_driver_framework:libhdf_utils",
    "hiviewdfx_hilog_native:libhilog",
    "input_device_driver:hdi_input",
    "ipc:ipc_single",
  ]
}

group("unittest") {
  testonly = true
  deps = []

  deps += [
    ":test_capacity_shutdown",
    ":test_hdisrv",
    ":test_input_event",
  ]
}
